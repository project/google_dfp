<?php

/**
 * @file
 * Defines a site-wide tier.
 */

$plugin = array(
  'class' => '\Drupal\google_dfp\Plugin\GoogleDfp\Tier\SiteWide',
);

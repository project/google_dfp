<?php

/**
 * @file
 * Defines a page-title tier.
 */

$plugin = array(
  'class' => '\Drupal\google_dfp\Plugin\GoogleDfp\Tier\PageTitle',
);

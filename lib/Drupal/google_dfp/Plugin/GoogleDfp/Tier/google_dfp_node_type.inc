<?php

/**
 * @file
 * Defines a node type tier.
 */

$plugin = array(
  'class' => '\Drupal\google_dfp\Plugin\GoogleDfp\Tier\NodeType',
);

<?php

/**
 * @file
 * Defines a node term tier.
 */

$plugin = array(
  'class' => '\Drupal\google_dfp\Plugin\GoogleDfp\Tier\NodeTerm',
);

<?php

/**
 * @file
 * Defines a placement tier.
 */

$plugin = array(
  'class' => '\Drupal\google_dfp\Plugin\GoogleDfp\Tier\Placement',
);

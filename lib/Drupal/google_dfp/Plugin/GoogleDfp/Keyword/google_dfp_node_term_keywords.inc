<?php

/**
 * @file
 * Defines a node term keywords plugin.
 */

$plugin = array(
  'class' => '\Drupal\google_dfp\Plugin\GoogleDfp\Keyword\NodeTerm',
);
